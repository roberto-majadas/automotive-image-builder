version: '2'

# We have loaded the variables specified in the manifest, the defaults
# and in the per-arc/target/image-type includes. We can now do optional
# things that depend on what was set in that.

mpp-vars:
  newline: "\n"
  # Backwards compatibility by checking if the deprecated kernel_rpm variable is set
  kernel_core_package:
    mpp-if: locals().get('kernel_rpm')
    then:
      mpp-eval: kernel_rpm + "-core"
    else:
      mpp-eval: kernel_package + "-core"
  kernel_devel_rpm:
    mpp-if: locals().get('kernel_rpm')
    then:
      mpp-eval: kernel_rpm + "-devel"
    else:
      mpp-eval: kernel_package + "-devel" if not kernel_version else kernel_package + "-devel-" + kernel_version
  kernel_rpm:
    mpp-if: locals().get('kernel_rpm')
    then:
      mpp-eval: kernel_rpm
    else:
      mpp-eval: kernel_package if not kernel_version else kernel_package + "-" + kernel_version
  use_kernel_debug_package:
    mpp-if: "'debug' in locals().get('kernel_package')"
    then: true
  # use_bluechi is a backwards compat option that enables both use_bluechi_agent and use_bluechi_controller
  # Also for historic reasons use_qm also currenlty implies both bluechi angent and controller
  use_bluechi_agent:
    mpp-eval: use_bluechi_agent or locals().get('use_bluechi') or use_qm
  use_bluechi_controller:
    mpp-eval: use_bluechi_controller or locals().get('use_bluechi') or use_qm
  distro_gpg_keys:
    mpp-if: locals().get('distro_gpg_keys')
    then:
      mpp-eval: distro_gpg_keys
    else:
      mpp-format-string: |
        $centos_gpg_key
        $redhat_gpg_key
  # We need to define this ahead of image-ostree.ipp.yml for use below
  use_ostree:
    mpp-eval: use_ostree or image_mode == 'image'
  _dash_escape: "\\x2d"
  boot_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2duuid-{bootfs_uuid.replace("-", _dash_escape)}.target

      [Mount]
      What=/dev/disk/by-uuid/$bootfs_uuid
      Where=/boot
      Type=ext4

      [Install]
      RequiredBy=local-fs.target
  boot_efi_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2dlabel-ESP.target

      [Mount]
      What=/dev/disk/by-label/ESP
      Where=/boot/efi
      Type=vfat

      [Install]
      RequiredBy=local-fs.target
  var_mount:
    mpp-format-string: |
      [Unit]
      Before=local-fs.target
      After=blockdev@dev-disk-by\\x2duuid-{varpart_uuid.replace("-", _dash_escape)}.target

      [Mount]
      What=/dev/disk/by-uuid/$varpart_uuid
      Where=/var
      Type=ext4

      [Install]
      RequiredBy=local-fs.target
  build_rpms:
    mpp-join:
      - mpp-eval: build_rpms
      - - mpp-if: use_luks
          then: cryptsetup
        - mpp-if: use_luks
          then: lvm2
        - mpp-if: use_aboot
          then: aboot-update
        - mpp-if: use_aboot
          then: aboot-deploy
        - mpp-if: use_composefs_signed
          then: openssl
      - - mpp-if: sign_kernel_modules
          then: $kernel_devel_rpm
  boot_rpms:
    mpp-join:
      - mpp-eval: boot_rpms
      - mpp-if: is_autoinit_supported and use_autoinit
        then:
          - util-linux-automotive
          - dracut-automotive
      - - $kernel_rpm
  base_rpms:
    mpp-join:
      - mpp-eval: base_rpms
      - - $linux_firmware_rpm
        - mpp-if: use_luks
          then: lvm2
        - mpp-if: use_aboot
          then: aboot-deploy
        - mpp-if: use_static_ip
          then: nmstate
        - mpp-if: not use_static_ip
          then: NetworkManager
        - mpp-if: use_bluechi_agent
          then: bluechi-agent
        - mpp-if: use_bluechi_controller
          then: bluechi-controller
        - mpp-if: use_qm
          then: qm
  dracut_modules:
    - mpp-if: is_autoinit_supported and use_autoinit
      then:
        systemd-initrd-automotive
  dracut_add_drivers:
    mpp-join:
      - mpp-eval: dracut_add_drivers
      - mpp-if: use_composefs
        then:
          - erofs
          - overlay
          - loop
  dracut_omit_modules:
    mpp-join:
      - mpp-eval: dracut_omit_modules
      - - mpp-if: not use_luks
          then: dm
  kernel_opts:
    mpp-join:
      - mpp-eval: kernel_opts
      - mpp-eval: extra_kernel_opts
      - mpp-if: consoles
        then:
          mpp-eval: "list(map(lambda c: 'console=' + c, consoles.split()))"
      - mpp-if: use_debug or use_kernel_debug_package
        then:
          - ignore_loglevel
          - earlycon
          # Configure the DMA unmap operations to invalidate IOMMU hardware TLBs synchronously.
          # This can catch some issues, however there is a performance penalty associated with
          # this option, so only enable this for debug builds.
          - iommu.strict=1
      - mpp-if: is_autoinit_supported and use_autoinit_root
        then:
          - mount-sysroot.root=$autoinit_root
          - mount-sysroot.rw
          - mpp-if: use_debug or use_kernel_debug_package
            then: mount-sysroot.debug
      - mpp-if: use_module_sig_enforce
        then:
          # Only allow modules with a valid signature to be loaded by the kernel.
          # https://www.kernel.org/doc/html/latest/admin-guide/module-signing.html#non-valid-signatures-and-unsigned-modules
          - module.sig_enforce=1
      - - systemd.show_status=auto
        # Staggered spin-up
        - libahci.ignore_sss=1
        # Enabling these slub_debug options to address a specific safety concern related to heap memory
        # integrity that only be achieved by setting the SLUB allocator to perform consistency checks.
        #   F = Sanity checks on
        #   P = Poisoning (object and padding)
        #   Z = Add guard areas (red zones) before and after each heap object
        # Note that enabling slub_debug will trigger the following warning on
        # boot up:
        # https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/blob/main/lib/vsprintf.c?ref_type=heads#L2230
        - slub_debug=FPZ
        - fsck.mode=skip
        # Disable rcu_normal_after_boot (because we want to delay the setting of rcu_normal to rcu-normal.service)
        - rcupdate.rcu_normal_after_boot=0
        # Enable rcu_expedited for faster boot (will be disabled by rcu-normal.service)
        - rcupdate.rcu_expedited=1
        # Disable all cgroups v1 interfaces
        - cgroup_no_v1=named,all
  load_modules:
    mpp-join:
      - mpp-eval: load_modules
      - - mpp-if: use_static_ip and static_ip_module
          then: $static_ip_module
  auto_modules_conf:
    mpp-format-string: |
      {newline.join(load_modules)}
  extra_image_copy:
    mpp-join:
      - mpp-eval: extra_image_copy
      - mpp-eval: locals().get('extra_image_copy_' + image_mode, [])
  image_repos:
    mpp-join:
      - mpp-eval: distro_repos
      - mpp-eval: target_repos
  build_repos:
    mpp-join:
      - mpp-eval: distro_repos
      - mpp-eval: target_build_repos
  image_devel_repos:
    mpp-join:
      - mpp-eval: distro_devel_repos
  containers_conf:
    mpp-format-string: |
      [containers]
      { "read_only = true" if containers_read_only else "" }

      default_capabilities = [
        "CHOWN",
        "DAC_OVERRIDE",
        "FOWNER",
        "FSETID",
        "KILL",
        "NET_BIND_SERVICE",
        "SETFCAP",
        "SETGID",
        "SETPCAP",
        "SETUID",
        "SYS_CHROOT"
      ]

      # A list of sysctls to be set in containers by default,
      # specified as "name=value",
      # for example:"net.ipv4.ping_group_range=0 0".
      #
      default_sysctls = [
        "net.ipv4.ping_group_range=0 0",
      ]

      # keyring tells the container engine whether to create
      # a kernel keyring for use within the container.
      #
      keyring = false
  qm_containers_storage_conf:
    mpp-format-string: |
      [storage]
      driver = "overlay"
      runroot = "/run/containers/storage"
      graphroot = "/var/lib/containers/storage"
      { "transient_store = true" if containers_transient_store else "" }

      [storage.options]
      additionalimagestores = [
         { '"' + qm_containers_extra_store + '"' if use_qm_containers_extra_store else "" }
      ]

      [storage.options.overlay]
      mountopt = "nodev,metacopy=on"
  image_enabled_services:
    mpp-join:
      - mpp-eval: image_enabled_services
      - - mpp-if: tmp_is_tmpfs
          then: tmp.mount
        - mpp-if: use_bluechi_agent
          then: bluechi-agent.service
        - mpp-if: use_bluechi_controller
          then: bluechi-controller.service
        - rcu-normal.service
        - mpp-if: use_efipart
          then: boot-efi.mount
        - mpp-if: use_bootpart
          then: boot.mount
        - mpp-if: use_separate_var
          then: var.mount
        - mpp-if: use_static_ip
          then: main-nmstate.service
  ostree_prepare_root_conf:
    mpp-format-string: |
      [sysroot]
      readonly=true
      [etc]
      { "transient=true" if use_transient_etc else "" }
      [composefs]
      enabled={"maybe" if not use_composefs else "signed" if use_composefs_signed else "yes" }
  build_info:
    mpp-format-string: |
      RELEASE="{release_name}"
      UUID="{image_uuid}"
      TIMESTAMP="{build_timestamp}"
      IMAGE_NAME="{name}"
      IMAGE_MODE="{image_mode}"
      IMAGE_TARGET="{target}"
  qm_importfile:
    mpp-if: use_qm
    then: "qm"
    else: "empty"
  bluechi_agent_conf:
    mpp-format-string: |
      [bluechi-agent]
      NodeName={ bluechi_nodename }
      ControllerHost= { bluechi_controller_host_ip }
  qm_bluechi_agent_conf:
    mpp-format-string: |
      [bluechi-agent]
      NodeName=qm.{ bluechi_nodename }
      ControllerHost= { bluechi_controller_host_ip }
  bluechi_controller_allowed_node_names:
    mpp-if: not bluechi_controller_allowed_node_names
    then:
      - $bluechi_nodename
      - mpp-if: use_qm
        then: qm.$bluechi_nodename
  bluechi_conf:
    mpp-format-string: |
      [bluechi-controller]
      AllowedNodeNames={ ','.join(bluechi_controller_allowed_node_names) }
  qm_subuid_content:
    mpp-format-string: |
      containers:{ qm_container_subuid }
  qm_subgid_content:
    mpp-format-string: |
      containers:{ qm_container_subgid }
  subuid_content_qm:
    mpp-if: use_qm
    then:
      mpp-format-string: |-
        qmcontainers:{qm_container_subuid}
        containers:{container_subuid}
    else: ""
  subgid_content_qm:
    mpp-if: use_qm
    then:
      mpp-format-string: |-
        qmcontainers:{qm_container_subgid}
        containers:{container_subgid}
    else: ""
  subuid_content: |-
    $subuid_content_qm
    $extra_subuid
  subgid_content: |-
    $subgid_content_qm
    $extra_subgid
  systemd_system_conf:
    mpp-format-string: |
      [Manager]
      DefaultTimeoutStartSec={systemd_timeout}
      DefaultTimeoutStopSec={systemd_timeout}
      DefaultTimeoutAbortSec={systemd_timeout}
      DefaultRestartSec={systemd_timeout}
      DefaultDeviceTimeoutSec={systemd_timeout}
  coredump_conf:
    mpp-format-string: |
      [Coredump]
      Storage={coredump_storage}
  rootpart_label:
    mpp-eval: aboot_partlabel if use_aboot else "root"
  main_nmstate:
    mpp-format-string: |
      ---
      interfaces:
        - name: {static_ip_iface}
          type: ethernet
          state: up
          ipv4:
            enabled: true
            dhcp: false
            address:
              - ip: {static_ip}
                prefix-length: {static_ip_prefixlen}
          ipv6:
            enabled: false
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-interface: {static_ip_iface}
            next-hop-address: {static_gw}
      dns-resolver:
        config:
          server:
            - {static_dns}
  main_nmstate_service:
    mpp-format-string: |
      [Unit]
      After=systemd-modules-load.service
      Before=systemd-udevd.service
      DefaultDependencies=no

      [Service]
      Type=oneshot
      ExecStart=/usr/bin/nmstatectl apply --kernel --no-verify /etc/main.nmstate
      RemainAfterExit=yes

      [Install]
      WantedBy=default.target
pipelines: []
